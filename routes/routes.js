var express = require('express');
var http = require('http');
var https = require('https');

var router = express.Router();

var googleMapsApiKey = 'AIzaSyAUga2DtLJ9BL5hE7y9Elrm-rf7jmnkaeY';

function validatePathRequest(path) {
	// proper format example: 10,10|20,20|30,30

	var requestValid = true;

	if (path == undefined || path.length === 0) {
		// path parameter was either not supplied or empty
		requestValid = false;
	} else {
		var pathArray = path.split("|");
		pathArray.forEach(function(item) {
			var latLngArray = item.split(",");
			// check that each coordinate set has two values
			if (latLngArray.length === 2 && latLngArray[0].length > 0 && latLngArray[1].length > 0) {
				var latInt = parseInt(latLngArray[0]);
				// latitude values cannot be less than -90 or more than 90
				if (latInt === NaN || latInt < -90 || latInt > 90) {
					requestValid = false;
				}
				var lngInt = parseInt(latLngArray[1]);
				// longitude values cannot be less than -180 or more than 180
				if (lngInt === NaN || lngInt < -180 || lngInt > 180) {
					requestValid = false;
				}
			} else {
				requestValid = false;
			}
		});
	}

	return requestValid;
}

function getJSON(options, onResult) {
    var protocol = options.port == 443 ? https : http;
    var request = protocol.request(options, function(res)
    {
        var output = '';
        res.setEncoding('utf8');

        res.on('data', function (chunk) {
            output += chunk;
        });

        res.on('end', function() {
            var obj = JSON.parse(output);
            onResult(res.statusCode, obj);
        });
    }).end();
};

function calculateDistanceInMeters(latLngArray) {
	var totalDistance = 0;

	var origin;
	var destination;
	latLngArray.forEach(function(item){
		if (destination != null) {
			// move destination to origin
			origin = destination;
		}
		destination = item;
		if (origin != null) {
			// calculate distance unless it is the first point
			var lat1Radians = convertDegreesToRadians(origin.latitude);
			var lng1Radians = convertDegreesToRadians(origin.longitude);
			var lat2Radians = convertDegreesToRadians(destination.latitude);
			var lng2Radians = convertDegreesToRadians(destination.longitude);
			var diffLatRadians = convertDegreesToRadians(destination.latitude - origin.latitude);
			var diffLngRadians = convertDegreesToRadians(destination.longitude - origin.longitude);

			// R is the radius of the earth in meters
			var R = 6371000;
			var a = Math.pow(Math.sin(diffLatRadians / 2), 2) + Math.cos(lat1Radians) * Math.cos(lat2Radians) * Math.pow(Math.sin(diffLngRadians / 2), 2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			var d = R * c;

			totalDistance += d;
		}
	});

	return totalDistance;
}

function convertDegreesToRadians(degrees) {
	return degrees * (Math.PI / 180);
}

/* GET routes endpoint */
router.get('/', function(req, res, next) {
	// validate path query string parameter
	if (!validatePathRequest(req.query.path)) {
		res.status(400).send({ error: 'Please supply a valid \'path\' query string parameter.' });
	}

	var path = req.query.path;

	// make request to Google Maps Roads API to get polyline
	var roadsOptions = {
		host: 'roads.googleapis.com',
		port: 443,
		path: '/v1/snapToRoads?interpolate=true&path=' + path + '&key=' + googleMapsApiKey,
		method: 'GET'
	};
	getJSON(roadsOptions, function(status, json) {
		if (status === 200) {
			var pathArray = [];
			var locationsString = '';
			json.snappedPoints.forEach(function(item, index, array) {
				pathArray.push({
					latitude: item.location.latitude,
					longitude: item.location.longitude,
					placeId: item.placeId
				});
				locationsString += item.location.latitude + ',' + item.location.longitude + '|'
			});
			locationsString = locationsString.substring(0, locationsString.length - 1);

			var distanceInMeters = calculateDistanceInMeters(pathArray);

			var elevationOptions = {
				host: 'maps.googleapis.com',
				port: 443,
				path: '/maps/api/elevation/json?key=' + googleMapsApiKey + '&locations=' + locationsString,
				method: 'GET'
			};
			getJSON(elevationOptions, function(status, json) {
				if (status === 200) {
					var minElevation = 0;
					var maxElevation = 0;
					var lastElevation;
					var elevationGain = 0;
					json.results.forEach(function(item, index, array) {
						pathArray[index].elevation = item.elevation;
						
						// update minElevation and maxElevation if necessary
						if (index === 0) {
							minElevation = item.elevation;
							maxElevation = item.elevation;
						} else if (item.elevation < minElevation) {
							minElevation = item.elevation;
						} else if (item.elevation > maxElevation) {
							maxElevation = item.elevation;
						}

						// if going uphill, add to cumulative gain
						if (item.elevation > lastElevation) {
							elevationGain += item.elevation - lastElevation;
						}
						lastElevation = item.elevation;
					});

					var route = {
						path: pathArray,
						elevation: {
							min: minElevation,
							max: maxElevation,
							gain: elevationGain
						}
					}
					res.send(JSON.stringify(route));
				}					
			});
		}
	});
});

module.exports = router;